<?php

/**  getParams
 * * Função para retornar Parâmetros e auxiliar na abstração de Expressões SQL
 * @param array $params - Parâmetros passados por um Array Associativo ['Chave' => 'Valor']
 *
 * @return array
 */
function getParams(array $params)
{
    $save['sql'] = '';
    foreach ($params as $key => $value) {
        $save['sql'] .= ' "' . $key . '" = :' . $key . ' ';
        $save['save'][':' . $key] = $value;
    }

    return $save;
}

/**  pdo
 * * Função executar Expressões SQL
 * @param class $PDO - Classe PDO Instanciada
 * @param string $sql - A Expressão SQL a ser executada
 * @param array $values - Valores passados por um Array Associativo ['Chave' => 'Valor']
 *
 * @return array Array Multidimensional Associativo contendo o retorno do SQL executado
 */
function pdo($PDO, $sql, $values)
{
    $stmt = pdoPrepare($PDO, $sql);
    $stmt = pdoBindValue($stmt, $values);
    return pdoExecute($stmt, $values);
}

/**  pdoPrepare
 * * Função para preparar a Expressão SQL
 * @param class $PDO - Classe PDO Instanciada
 * @param string $sql - A Expressão SQL a ser executada
 *
 * @return Retorna o Statment (stmt) da classe PDO com a SQL Preparada
 */
function pdoPrepare($PDO, $sql)
{
    return $PDO->prepare($sql);
}

/**  pdoBindValue
 * * Função para parametrizar um Statment da classe PDO com SQL Preparada
 * @param class $stmt - Statment da classe PDO com a SQL Preparada
 * @param array $values - Parâmetros e Valores passados em Array Multidimensional Associativo ['Chave' => 'Valor']
 *
 * @return Retorna o Statment (stmt) da classe PDO com a SQL Parametrizada
 */
function pdoBindValue($stmt, $values)
{
    foreach ($values as $k => $v) {
        $stmt->bindValue(':' . $k, $v);
    }
    return $stmt;
}

/**  pdoExecute
 * * Função para executar um Statment da classe PDO Parametrizada
 * @param class $stmt - Statment da classe PDO com a SQL Preparada
 * @param array $values - Parâmetros e Valores passados em Array Multidimensional Associativo ['Chave' => 'Valor']
 *
 * @return Retorna o Statment (stmt) da classe PDO executado
 */
function pdoExecute($stmt, array $values = array())
{
    return $stmt->execute($values);
}

<?php

include('DBSuporte.php');

/**  SQL INSERT
 * * Função para abstração do SQL INSERT
 * @param string $table - Tabela que terá dados inseridos
 * @param array $keyValue - Registros a serem inseridos, passados por um Array Associativo ['Nome do Campo' => 'Valor']
 *
 * @return void
 */
function create($table, array $keyValue)
{
    // Adquirindo parâmetros passados pelo array de Chave=>Valor
    $params = getParams($keyValue);

    // Separando parâmetros em campos e valores
    $fields = implode(',', array_keys($keyValue));
    $values = implode(',', array_keys($params['save']));

    // Montando SQL de INSERT
    $sql = 'INSERT INTO ' . $table . ' (' . $fields . ') VALUES (' . $values . ');';

    // Chamando $PDO do contexto global
    global $PDO;
    pdo($PDO, $sql, $params['save']); // Executando SQL
}

/**  SQL SELECT
 * * Função para abstração do SQL SELECT
 * @param string $table - Tabela que será atualizada
 * @param string $type - Tipo de retorno ('all', 'first' ou 'count')
 * @param array $configs - Configurações passadas por um Array Multidimensional Associativo ['Nome da Cláusula' => ['Nome do Campo' => 'Valor']] contendo:
 *              Cláusula['fields'] - Array com os Campos a serem retornados (* para todos)
 *              Cláusula['where'] - Array Associativo ['Nome do Campo' => 'Valor'] (apenas relação de igualdade)
 *              Cláusula['group by'] - Array com os Campos a serem agrupados
 *              Cláusula['order by'] - Array com os Campos a serem agrupados (pode ser concatenado um modificador ' DESC' para inverter a ordem)
 *              Cláusula['limit'] - Array unitário passando o valor da cláusula LIMIT
 *              Cláusula['offset'] - Array unitário passando o valor da cláusula OFFSET
 *
 * @return Array Multidimensional Associativo ['# registro' => [''Nome do Campo' => 'Valor']] contendo registros da tabela
 */
function read($table, $type = 'all', array $configs = array())
{
    // Inicializando variáveis
    $params['sql'] = '';
    $params['save'] = array();
    $fields = '';

    // Verificando se há Campos para montagem do SQL
    if (!isset($configs['fields'])) {
        $fields = ' * ';
    } else {
        $fields = implode(',', array_values($configs['fields']));
    }

    // Verificando cláusula WHERE para montagem do SQL
    if (isset($configs['where'])) {
        $ret = getParams($configs['where']); // Adquirindo parâmetros passados por $configs['where']
        $params['sql'] .= ' WHERE ' . $ret['sql']; // Montando cláusula WHERE para formar o SQL
        $params['save'] = array_merge($ret['save'], $params['save']);
    }

    // Verificando cláusula GROUP BY para montagem do SQL
    if (isset($configs['group by'])) {
        $ret = implode(',', $configs['group by']); // Adquirindo campos passados pelo array $configs['group by']
        $params['sql'] .= ' GROUP BY ' . $ret['sql']; // Montando cláusula GROUP BY para formar o SQL
    }

    // Verificando cláusula ORDER BY para montagem do SQL
    if (isset($configs['order by'])) {
        $ret = implode(',', $configs['order by']); // Adquirindo campos passados pelo array $configs['order by']
        $params['sql'] .= ' ORDER BY ' . $ret['sql']; // Montando cláusula ORDER BY para formar o SQL
    }

    // Verificando cláusula LIMIT para montagem do SQL
    if (isset($configs['limit'])) {
        $params['sql'] .= ' LIMIT ' . $configs['limit']; // Montando cláusula LIMIT e seu valor para formar o SQL
    }

    // Verificando cláusula OFFSET para montagem do SQL
    if (isset($configs['offset'])) {
        $params['sql'] .= ' OFFSET ' . $configs['offset']; // Montando cláusula OFFSET e seu valor para formar o SQL
    }

    // Montando SQL com os CAMPOS e CLÁUSULAS identificados
    $sql = 'SELECT ' . $fields . ' FROM ' . $table . ' ' . $params['sql'] . ';';

    // Chamando $PDO do contexto global
    global $PDO;
    $res = pdo($PDO, $sql, $params['save']); // Executando SQL

    if ($type == 'first') {
        return $res->fetch(PDO::FETCH_ASSOC);
    } elseif ($type == 'all') {
        return $res->fetchAll(PDO::FETCH_ASSOC);
    } elseif ($type == 'count') {
        return $res->rowCount();
    }
}

/**  SQL UPDATE
 * * Função para abstração do SQL UPDADE
 * @param string $table - Tabela que terão registros atualizados
 * @param array $conditions - Condições do WHERE passados por Array Associativo ['Nome do Campo' => 'Valor']
 * @param array $fields - Campos e seus Valores alterados, passados por Array Associativo ['Nome do Campo' => 'Valor']
 *
 * @return void
 */
function update($table, array $conditions, array $fields)
{
    // Adquirindo Campos passados por array
    $save = getParams($fields);

    $conditions = getParams($conditions);

    $save['save'] = array_merge($save['save'], $conditions['save']);

    // Montagem do SQL de UPDATE
    $sql = 'UPDATE ' . $table . ' SET ' . $save['sql'] . ' WHERE ' . $conditions['sql'] . ';';

    // Chamando $PDO do contexto global
    global $PDO;
    pdo($PDO, $sql, $save['save']);
}

/**  SQL DELETE
 * * Função para abstração do SQL DELETE
 * @param string $table - Tabela que terão registros apagados
 * @param array $conditions - Condições do WHERE passados por array associativo ['Nome do Campo' => 'Valor']
 *
 * @return void
 */
function delete($table, array $conditions)
{
    // Adquirindo condições passadas pelo array de Chave=>Valor (Campo => Valor)
    $save = getParams($conditions);

    // Montagem do SQL de DELETE
    $sql = 'DELETE FROM ' . $table . ' WHERE ' . $save['sql'] . ';';

    // Chamando $PDO do contexto global
    global $PDO;
    pdo($PDO, $sql, $save['save']);
}

/** SELECT
 * ! Função antiga para abstração do SQL SELECT
 * @param string $table - Tabela que retornará dados
 * @param int id = null - Valor do ID para trazer um único registro
 * *
function select($table, $id = null)
{
    global $PDO;
    $sql = "SELECT * FROM {$table}";
    if ($id != null) {
        $sql .=  " WHERE id = :id";
        $stmt = $PDO->prepare($sql);
        $stmt->bindParam(':id', $id);
        $res = $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    } else {
        $res = $PDO->query($sql);
        return $res->fetchAll(PDO::FETCH_ASSOC);
    }
}
*/

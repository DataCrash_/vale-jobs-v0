<?php

/**  DB Connection
 * * Função para Conectar a um Banco de Dados usando a classe PDO
 * @param string $dbms - SGBD utilizado (exemplo: mysql)
 * @param string $host - Endereço onde conectar ao SGBD
 * @param string $dbname - Nome do Banco de Dados a ser conectado
 * @param string $user - Nome do Usuário para Login no Banco
 * @param string $password - Senha do Usuário para Login no Banco (padrão = '')
 * @param string $option - Opções de conexão ao Bando de Dados (padrão = '')
 *
 * @return Retorna a classe PDO Instanciada com o Banco Conectado
 */
function Connection($dbms, $host, $dbname, $user, $password = '', $options = '')
{
    try {
        $PDO = new PDO($dbms . ":host=" . $host . ';dbname=' . $dbname, $user, $password, $options);
        $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $PDO->exec("set names utf8");
    } catch (PDOException $e) {
        echo "Erro ao conectar ao Banco de Dados!<br>" . $e;
    }
    return $PDO;
}

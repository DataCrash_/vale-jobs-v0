<?php

include('../../Core/DBConnection.php');

/**  DB Consts
 * * Constantes de Sistema para Conexão ao Banco de Dados
 */
define('DBMS', 'mysql');
define('HOST', 'localhost');
define('DBNAME', 'aula_senac');
define('USER', 'root');
define('SENHA', '');

/**  PDO Global
 * * Conectando ao Banco de Dados e Instanciando PDO de contexto global
 */
$PDO = Connection(DBMS, HOST, DBNAME, USER, SENHA);
